{Adapter,Robot,TextMessage,EnterMessage,LeaveMessage} = require 'hubot'

HTTPS        = require 'https'
EventEmitter = require('events').EventEmitter
net          = require('net')
tls          = require('tls')

class Celly extends Adapter
  send: (envelope, strings...) ->
    strings.forEach (str) =>
      @bot.write envelope.room, {"type": "message", "content": str}

  reply: (envelope, strings...) ->
    strings.forEach (str) =>
      @send envelope, "@#{envelope.user.name} #{str}"

  run: ->
    self = @
    token = process.env.HUBOT_CELLY_TOKEN
    rooms = process.env.HUBOT_CELLY_ROOMS.split(',')

    bot = new CellyClient()
    console.log bot

    ping = (room)->
      setInterval ->
        bot.write room, {type: "ping"}
      , 25000

    bot.on "Ready", (room)->
      message = {"room": room, "token": token, "type": "connect"}
      bot.write room, message
      ping room

    bot.on "Users", (message)->
      for user in message.users
        self.userForId(user.id, user)

    bot.on "TextMessage", (room, message)->
      unless self.robot.name.toUpperCase() == message.user.name.toUpperCase()
        # Replace "@mention" with "mention: ", case-insensitively
        name_escape_regexp = new RegExp("[.*+?|()\\[\\]{}\\\\]", "g")
        escaped_name = self.robot.name.replace( name_escape_regexp, "\\$&")

        name_regexp = new RegExp "^@#{escaped_name}", 'i'
        content = message.content.replace(name_regexp, self.robot.name)

        self.receive new TextMessage self.userForMessage(room, message), content

    bot.on "EnterMessage", (room, message) ->
      unless self.robot.name.toUpperCase() == message.user.name.toUpperCase()
        self.receive new EnterMessage self.userForMessage(room, message)

    bot.on "LeaveMessage", (room, message) ->
      unless self.robot.name.toUpperCase() == message.user.name.toUpperCase()
        self.receive new LeaveMessage self.userForMessage(room, message)

    for room in rooms
      bot.sockets[room] = bot.createSocket(room)

    @bot = bot

    self.emit "connected"

  userForMessage: (room, message)->
    author = @userForId(message.user.id, message.user)
    author.room = room
    author

exports.use = (robot) ->
  new Celly robot

class CellyClient extends EventEmitter
  constructor: ->
    @host          = 'http://cel.ly/api'
    @encoding      = 'utf8'
    @port          = 80
    @sockets       = {}

  createSocket: (room) ->
    self = @

    request = require('superagent')

    socket = tls.connect @port, @host, ->
      console.log("Connected to room #{room}.")
      self.emit "Ready", room

    #callback
    socket.on 'data', (data) ->
      for line in data.split '\n'
        message = if line is '' then null else JSON.parse(line)

        if message
          console.log "From room #{room}: #{line}"
          if message.type == "users"
            self.emit "Users", message
          if message.type == "message"
            self.emit "TextMessage", room, message
          if message.type == "join"
            self.emit "EnterMessage", room, message
          if message.type == "leave"
            self.emit "LeaveMessage", room, message
          if message.type == "error"
            self.disconnect room, message.message

    socket.addListener "eof", ->
      console.log "eof"
    socket.addListener "timeout", ->
      console.log "timeout"
    socket.addListener "end", ->
      console.log "end"

    socket.setEncoding @encoding

    socket

  handleResponse: (data) ->
    console.log "response", data

  poll: (interval) ->
    self = @
    setInterval => @handleResponse, interval

  write: (room, args) ->
    self = @
    @sockets[room]

    if @sockets[room].readyState != 'open'
      return @disconnect room, 'cannot send with readyState: ' + @sockets[room].readyState

    message = JSON.stringify(args)
    console.log "To room #{room}: #{message}"

    @sockets[room].write message, @encoding

  disconnect: (room, why) ->
    if @sockets[room] != 'closed'
      @sockets[room]
      console.log 'disconnected (reason: ' + why + ')'