# Celly Adapter for hubot

## Description

A Hubot adapter for [Cel.ly](http://cel.ly/)

## Hubot Compatibility

This version of the cel.ly adaptor is for Hubot 2.7.0

## Installation

* Add `hubot-celly` as a dependency in your hubots `package.json`
* Run `npm install` in your hubots directory.
* Run hubot with `bin/hubot -a celly`

## Usage

You need to set a few environment variables in order for it to work

**NB** If the nick isn't already registered on the jabbr host you are using,
the nick will need to be registered first.

### GNU/Linux

    % export HUBOT_CELLY_NICK="HubotNick"

    % export HUBOT_CELLY_PASSWORD="ThePassword"

    % export HUBOT_CELLY_ROOMS="me,Room1,Room2,Room3" # rooms in hubot are cells on cel.ly - names are case insensitive

    # Optional, you can choose the transport to use to connect to the server with. Default is serverSentEvents
    % export HUBOT_CELLY_TRANSPORT="longPolling"

### Heroku

    % heroku config:add HUBOT_CELLY_NICK="HubotNick"

    % heroku config:add HUBOT_CELLY_PASSWORD="ThePassword"

    % heroku config:add HUBOT_CELLY_ROOMS="Room1,Room2,Room3"

    # Optional, you can choose the transport to use to connect to the server with. Default is serverSentEvents
    % heroku config:add HUBOT_CELLY_TRANSPORT="longPolling"

## Contributing

Contributions are welcome. To get your work considered please do the following:

1. Fork this project
2. Create a feature/bug fix branch
3. Hack away, committing often and frequently
4. Push your branch up to your fork
5. Submit a pull request

## License

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://sam.zoy.org/wtfpl/COPYING for more details.
